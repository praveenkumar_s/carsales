package com.app.cars.controller;

import com.app.cars.beans.Car;
import com.app.cars.svc.InventorySvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomePageController extends AbstractController {
    private static Logger logger = Logger.getLogger(HomePageController.class);
    private InventorySvc inventorySvc;
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String action = request.getParameter("action");
        logger.info("action=" + action);

        if(action!=null && !action.equalsIgnoreCase("")) {
            if(action.equalsIgnoreCase("searchModel")) {
                String model = request.getParameter("model");

                List<Car> cars = inventorySvc.searchCarModels(model);
                ObjectMapper mapper = new ObjectMapper();
                String output = mapper.writeValueAsString(cars);

                PrintWriter writer = response.getWriter();
                writer.print(output);
                writer.flush();
                writer.close();
                return null;
            }

            return null;
        } else {
            logger.info("Running handleRequestInternal");
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("availableCarModels", inventorySvc.getAvailableCarModels());
            logger.info("Map is = \n" + map);
            logger.info("Sending to homepage.htm");
            return new ModelAndView("homePage", map);
        }
    }

    public void setInventorySvc(InventorySvc inventorySvc) {
        this.inventorySvc = inventorySvc;
    }
}
